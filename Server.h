#ifndef SERVER_H
#define SERVER_H

#include <string>
#include <list>
#include <cstdint>
#include <websocketpp/config/asio_no_tls.hpp>
#include <websocketpp/server.hpp>
#include <websocketpp/connection.hpp>

//!
//! \brief Network server class.
//!
class CServer
{
public:
    CServer();

    //!
    //! \brief Run server.
    //! \param _addr Network address.
    //! \param _port Network port.
    //! \return exit code.
    //!
    int Run(const std::string &_addr, uint16_t _port, size_t _max_clients = 10);

    //!
    //! \brief Shutdown server.
    //!
    void Shutdown();

private:
    typedef websocketpp::lib::asio::io_service ServiceT;
    typedef websocketpp::lib::asio::signal_set SignalSetT;
    typedef websocketpp::lib::error_code ErrorCodeT;
    typedef websocketpp::server<websocketpp::config::asio> ServerT;
    typedef websocketpp::connection_hdl ConnectionHandlerT;
    typedef std::list< websocketpp::lib::shared_ptr<void> > ConnectionArrayT;

    // Signals (SIGINT, SIGTERM) handler
    void SignalHandler(const ErrorCodeT &_error_code, int _signal_number);

    // Connection open handler
    void OpenHandler(ConnectionHandlerT _connection_hdl);

    // Connection close handler
    void CloseHandler(ConnectionHandlerT _connection_hdl);

    // Message handler
    void MessageHandler(ConnectionHandlerT _connection_hdl, ServerT::message_ptr _message);

    // Client information (ip, port)
    std::string ClientInfo(ConnectionHandlerT _connection_hdl);

    ServiceT m_Service;         // Async service
    SignalSetT m_SignalSet;     // Signals receiver
    ServerT m_Server;           // Server
    ConnectionArrayT m_Clients; // Clients connection handlers
    size_t m_MaxClients;        // Maximum clients number
};

#endif // SERVER_H
