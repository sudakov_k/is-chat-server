#include "Message.h"

// Aliases
using std::string;

bool CMessage::CheckMessage(const string &_data)
{
    bool success = false;   // Check success

    // Minimum data size is 4 bytes (empty name and text sections)
    if (_data.size() >= 4)
    {
        // Name size: bytes 0, 1
        size_t name_size = ToU16(_data[0], _data[1]);

        // Text size: bytes 2, 3
        size_t text_size = ToU16(_data[2], _data[3]);

        // Check message size
        if (_data.size() == (name_size + text_size + 4))
        {
            // Correct message
            success = true;
        }
    }

    return success;
}

uint16_t CMessage::ToU16(char _high, char _low)
{
    // char -> unsigned char is needed for negative values

    // Low part
    uint16_t value = static_cast<unsigned char>(_low);

    // High part
    value |= (static_cast<uint16_t>(static_cast<unsigned char>(_high)) << 8) & UINT16_C(0xFF00);

    return value;
}
