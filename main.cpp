#include <iostream>
#include "CmdArgs.h"
#include "Server.h"
#include "Log.h"

using namespace std;

int main(int argc, char *argv[])
{
    int exit_code = 1;  // exit code
    CCmdArgs cmd_args;  // command line parser

    if (cmd_args.Parse(argc, argv))
    {
        // Open log file
        if (g_Log.Open(cmd_args.GetLogFile()))
        {
            // command line parse success, start server
            CServer server;
            exit_code = server.Run(cmd_args.GetAddr(), cmd_args.GetPort());
        }
        else
        {
            cerr << "error: open log file: \"" << cmd_args.GetLogFile() << "\"" << endl;
        }
    }
    else
    {
        // command line parse error or help
        exit_code = cmd_args.GetExitCode();
    }

    return exit_code;
}
