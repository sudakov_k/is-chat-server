#include "Server.h"
#include "Message.h"
#include "Log.h"

// Aliases
namespace placeholders = websocketpp::lib::placeholders;
namespace opcode = websocketpp::frame::opcode;
namespace close_status = websocketpp::close::status;
using std::string;
using std::to_string;
using websocketpp::lib::bind;
using websocketpp::exception;
using websocketpp::log::alevel;

CServer::CServer() :
    m_Service(),
    m_SignalSet(m_Service),
    m_Server(),
    m_Clients(),
    m_MaxClients()
{
    m_Server.init_asio(&m_Service);
}

int CServer::Run(const string &_addr, uint16_t _port, size_t _max_clients)
{
    // exit code: 0 - ok, other - fail
    int exit_code = -1;

    // Set max clients
    m_MaxClients = _max_clients;

    // Log started parameters
    g_Log.Insert("run, addr: \"" + _addr + "\", port: \"" + to_string(_port) + "\"");

    try
    {
        // Disable console logger
        m_Server.clear_access_channels(alevel::all);
        m_Server.clear_error_channels(alevel::all);

        // Configure signal receiver
        m_SignalSet.add(SIGINT);
        m_SignalSet.add(SIGTERM);
        m_SignalSet.async_wait(bind(&CServer::SignalHandler, this, placeholders::_1, placeholders::_2));

        // Configure server
        m_Server.set_open_handler(bind(&CServer::OpenHandler, this, placeholders::_1));
        m_Server.set_close_handler(bind(&CServer::CloseHandler, this, placeholders::_1));
        m_Server.set_message_handler(bind(&CServer::MessageHandler, this, placeholders::_1, placeholders::_2));
        m_Server.listen(_addr, to_string(_port));
        m_Server.start_accept();

        // Start async service
        m_Service.run();

        // Server stopped, exit code 0
        exit_code = 0;
    }
    catch (const exception &e)
    {
        g_Log.Insert(e.what());
    }
    catch (...)
    {
        g_Log.Insert("unknown exception");
    }

    return exit_code;
}

void CServer::Shutdown()
{
    // Log shutdown
    g_Log.Insert("shutdown");

    // Stop service, cances all async operations
    if (!m_Service.stopped())
    {
        m_Service.stop();
    }
}

void CServer::SignalHandler(const ErrorCodeT &_error_code, int /* _signal_number */)
{
    // Shutdown
    if (!_error_code)
    {
        Shutdown();
    }
}

void CServer::OpenHandler(ConnectionHandlerT _connection_hdl)
{
    // Check clients number
    if (m_Clients.size() < m_MaxClients)
    {
        // New client connection
        m_Clients.push_back(_connection_hdl.lock());

        // Log new connection
        g_Log.Insert("connected: " + ClientInfo(_connection_hdl));
    }
    else
    {
        ErrorCodeT ec;  // Close error code
        m_Server.close(_connection_hdl, close_status::try_again_later, "", ec);

        // Log connections limit
        g_Log.Insert("connections limit");
    }
}

void CServer::CloseHandler(ConnectionHandlerT _connection_hdl)
{
    // Client close connection, remove client
    m_Clients.remove(_connection_hdl.lock());
    g_Log.Insert("disconnected: " + ClientInfo(_connection_hdl));
}

void CServer::MessageHandler(ConnectionHandlerT _connection_hdl, ServerT::message_ptr _message)
{
    if (_message)
    {
        // Received message opcode must be "binary"
        if (_message->get_opcode() == opcode::binary)
        {
            // Check message
            if (CMessage::CheckMessage(_message->get_payload()))
            {
                // Correct message, send message to all clients
                for (ConnectionHandlerT client : m_Clients)
                {
                    ErrorCodeT ec;  // send error code
                    m_Server.send(client, _message->get_payload(), _message->get_opcode(), ec);

                    // Send error
                    if (ec)
                    {
                        // Log send error
                        g_Log.Insert(ec.message());
                    }
                }
            }
            else
            {
                // Log incorrect message
                g_Log.Insert("received incorrect message, " + ClientInfo(_connection_hdl));
            }
        }
        else
        {
            // Log incorrect message
            g_Log.Insert("received incorrect message, " + ClientInfo(_connection_hdl));
        }
    }
}

string CServer::ClientInfo(ConnectionHandlerT _connection_hdl)
{
    string str;     // client info string

    try
    {
        // Get connection from handler
        ServerT::connection_ptr con = m_Server.get_con_from_hdl(_connection_hdl);

        // Get client endpoint (string)
        str = con->get_remote_endpoint();
    }
    catch (const exception & /* e */)
    {
        str = "unknown";
    }

    return str;
}
