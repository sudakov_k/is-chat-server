# is-chat-server #
## Overview ##
is-chat-server - asynchronous chat server based on [WebSocket++](https://github.com/zaphoyd/websocketpp) library.

## Usage ##
```
#!text

./is-chat-server [args]
args:
-h, --help : help;
-a ADDR, --addr ADDR : network interface address, default: "0.0.0.0";
-p PORT, --port PORT : network port, default: "8080";
-l LOG, --log LOG : log file name, default: "server.log".
```

## Build ##
Dependency:

* WebSocket++ (libwebsocketpp-dev), headers only;
* Asio C++ Library (libasio-dev), headers only;
* POSIX Threads (libpthread-stubs0-dev);
* C++11 support.

Build using makefile and 3rdparty libs:
```
#!sh

cd is-chat-server
make
```

Build using makefile and system libs:
```
#!sh

cd is-chat-server
make USE_SYSTEM_LIBS=1
```

Build using qmake (system libs):
```
#!sh

mkdir build
cd build
qmake ../is-chat-server
make

```
