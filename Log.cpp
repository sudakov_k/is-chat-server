#include <ctime>
#include "Log.h"

using std::string;
using std::ios_base;

CLog g_Log;

CLog::CLog() :
    m_Lock(),
    m_Stream()
{
}

CLog::~CLog()
{
    // no need close file, std::ofstream automatic close on destroy.
}

bool CLog::Open(const string &_file_name)
{
    // Lock access
    m_Lock.lock();

    // Close old file
    if (m_Stream.is_open())
    {
        m_Stream.close();
    }

    // Open new file
    m_Stream.open(_file_name, ios_base::out | ios_base::app);

    bool success = m_Stream.is_open();

    // Unlock access
    m_Lock.unlock();

    return success;
}

void CLog::Close()
{
    // Lock access
    m_Lock.lock();


    // Close file
    if (m_Stream.is_open())
    {
        m_Stream.close();
    }

    // Unlock access
    m_Lock.unlock();
}

void CLog::Insert(const string &_str)
{
    // Lock access
    m_Lock.lock();

    // Insert message
    if (m_Stream.is_open() && m_Stream.good())
    {
        m_Stream << "[" << DateTimeNow() << "] "
                 << _str << "\n";
    }

    // Unlock access
    m_Lock.unlock();
}

string CLog::DateTimeNow()
{
    constexpr size_t buf_max = 32;  // Buffer maximum characters, including null-terminator
    size_t buf_size = 0;            // Character number in buffer
    char buf[buf_max] { '\0' };     // Character buffer

    // raw time
    time_t raw_time = time(nullptr);
    tm utc_time;

    // Convert raw time to UTC time structure
    if (gmtime_r(&raw_time, &utc_time) != nullptr)
    {
        // Convert UTC time to string
        buf_size = strftime(buf, buf_max, "%F %T", &utc_time);
    }

    return string(buf, buf_size);
}
