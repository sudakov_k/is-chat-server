CXX = g++
LINK = g++

CXXFLAGS = -Wall -Wextra -std=c++11 -O3 -DASIO_STANDALONE
LDFLAGS = -s
LIBS = -lpthread

OBJS = main.o CmdArgs.o Log.o Message.o Server.o

ifndef USE_SYSTEM_LIBS
	CXXFLAGS += -I"3rdparty/websocketpp-0.7.0" -I"3rdparty/asio-1.10.6/include"
endif

all: is-chat-server

is-chat-server: $(OBJS)
	$(LINK) $(LDFLAGS) -o "is-chat-server" $(OBJS) $(LIBS)

%.o: %.cpp
	$(CXX) -c $(CXXFLAGS) -o "$@" "$<"

clean:
	rm is-chat-server $(OBJS)
