#include <iostream>
#include <getopt.h>
#include "CmdArgs.h"

using namespace std;

CCmdArgs::CCmdArgs() :
    m_Addr("0.0.0.0"),
    m_Port(8080),
    m_LogFile("server.log"),
    m_ExitCode(0)
{

}

bool CCmdArgs::Parse(int _argc, char * const *_argv)
{
    // parser use GNU extension: getopt_long,
    // boost/program_options is more correct

    // long options:
    // --help, no arguments;
    // --addr, argument required;
    // --port, argument requered;
    // --log, argument required.
    const option long_opt[] = {
        { "help", no_argument, nullptr, 'h' },
        { "addr", required_argument, nullptr, 'a' },
        { "port", required_argument, nullptr, 'p' },
        { "log", required_argument, nullptr, 'l' },
        { nullptr, 0, nullptr, 0 }
    };

    bool success = true;    // parse success
    int short_opt;          // short option

    // Parse command line
    // loop, exit when one is:
    // getopt_long return -1;
    // success is false.
    //
    // note:
    // (short_opt = getopt_long(...)) is not comparsion, is assignment.
    while (
           ((short_opt = getopt_long(_argc, _argv, "ha:p:l:", long_opt, nullptr)) != -1)
           && success
           )
    {
        switch (short_opt)
        {

        // --help: print help and exit
        case 'h':
            if (_argc > 0)
            {
                Help(_argv[0]);
            }

            success = false;
            // m_ExitCode is 0;
            break;

        // --addr: network address
        case 'a':
            m_Addr = optarg;
            break;

        // --port: network port
        case 'p':
            if (!StrToPort(optarg, m_Port))
            {
                cerr << "error: \"" << optarg << "\" is not port" << endl;
                success = false;
                m_ExitCode = 1;
            }

            break;

        // --log: log file name
        case 'l':
            m_LogFile = optarg;
            break;

        // Other keys, error
        /* case '?': */
        default:
            if (_argc > 0)
            {
                Help(_argv[0]);
            }

            success = false;
            m_ExitCode = 1;
            break;
        }

    }

    return success;
}

string CCmdArgs::GetAddr() const
{
    return m_Addr;
}

uint16_t CCmdArgs::GetPort() const
{
    return m_Port;
}

string CCmdArgs::GetLogFile() const
{
    return m_LogFile;
}

int CCmdArgs::GetExitCode() const
{
    return m_ExitCode;
}

bool CCmdArgs::StrToPort(const string &_str, uint16_t &_port)
{
    bool success = false;

    // Conversion string to uint16_t function does not exist in std.
    // Variant 1:
    // 1. convert string to unsigned long;
    // 2. convert unsigned long to uint16_t.
    //
    // Variant 2:
    // use sscanf function.
    static_assert(sizeof(uint16_t) <= sizeof(unsigned long), "uint16_t > unsigned long, using stoul is incorrect");

    try
    {
        unsigned long port = stoul(_str);

        // Port values is [0 ... UINT16_MAX]
        if (port <= UINT16_MAX)
        {
            _port = static_cast<uint16_t>(port);
            success = true;
        }
    }
    catch (...)
    {
        // conversion fail
    }

    return success;
}

void CCmdArgs::Help(const string &prog_name) const
{
    cout << prog_name << " [args]\n"
         << "args:\n"
         << "-h, --help           : help;\n"
         << "-a ADDR, --addr ADDR : network interface address, default: \"0.0.0.0\";\n"
         << "-p PORT, --port PORT : network port, default: \"8080\";\n"
         << "-l LOG, --log LOG    : log file name, default: \"server.log\".\n"
         << flush;
}
