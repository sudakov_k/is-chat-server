TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

DEFINES += ASIO_STANDALONE

LIBS += -lpthread

SOURCES += main.cpp \
    CmdArgs.cpp \
    Server.cpp \
    Log.cpp \
    Message.cpp

HEADERS += \
    CmdArgs.h \
    Server.h \
    Log.h \
    Message.h
