#ifndef CMDARGS_H
#define CMDARGS_H

#include <string>
#include <cstdint>

//!
//! \brief Command line parser.
//!
class CCmdArgs
{
public:
    //!
    //! \brief Construct default values.
    //!
    CCmdArgs();

    //!
    //! \brief Parse command line arguments.
    //! \param _argc Arguments number.
    //! \param _argv Arguments list.
    //! \return true on success.
    //!
    //! Help is no success, but GetExitCode() return 0.
    //!
    bool Parse(int _argc, char * const *_argv);

    //!
    //! \brief Get network address.
    //! \return Address.
    //!
    std::string GetAddr() const;

    //!
    //! \brief Get network port.
    //! \return Port.
    //!
    uint16_t GetPort() const;

    //!
    //! \brief Get log file name.
    //! \return Log file name.
    //!
    std::string GetLogFile() const;

    //!
    //! \brief Get exit code.
    //! \return 0 on success.
    //!
    int GetExitCode() const;

private:
    // Conversion string to network port
    // return success
    static bool StrToPort(const std::string &_str, uint16_t &_port);

    // Print help
    void Help(const std::string &prog_name) const;

    std::string m_Addr;     // network address
    uint16_t m_Port;        // network port
    std::string m_LogFile;  // log file name
    int m_ExitCode;         // exit code
};

#endif // CMDARGS_H
