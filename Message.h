#ifndef MESSAGE_H
#define MESSAGE_H

#include <string>
#include <cstdint>

//!
//! \brief Message parser class.
//!
//! Message format:
//! [ name_size ] [ text_size ] [ name ] [ text ]
//! - name_size: 2 bytes, size name section;
//! - text_size: 2 bytes, size text section;
//! - name: client name, not null-terminated;
//! - text: client text, not null-terminated.
//!
class CMessage
{
public:
    //!
    //! \brief Check message.
    //! \param _data Raw data.
    //! \return Check success.
    //!
    static bool CheckMessage(const std::string &_data);

    //!
    //! \brief Name section maximum size.
    //!
    static constexpr size_t nameMax = UINT16_MAX;

    //!
    //! \brief Text section maximum size.
    //!
    static constexpr size_t textMax = UINT16_MAX;

private:
    // Convert raw data to uint16_t value
    static uint16_t ToU16(char _high, char _low);
};

#endif // MESSAGE_H
